# Menggunakan API Pada Proyek Belajar

Mungkin kalian bertanya-tanya bagaimana jika kita ingin menambahkan data yang banyak, apakah harus menambah kode yang banyak secara statis di kelas `DataDummy.java`. Tentu saja tidak mungkin kita menambahkan data secara manual di code `DataDummy.java`, kita dapat menggunakan data dari server dengan menggunakan API, sehingga data akan lebih dinamis dan bisa berintegrasi dengan platform lainnya seperti aplikasi web. 

Pada Proyek Belajar kali ini kita akan membuat data yang akan digunakan berasal dari server menggunakan API, disini mungkin kita akan membagi dua tahap implementasi agar tidak terlalu panjang, kita akan mengambil data untuk menampilkan list movie dan mengambil data untuk menampilkan detail movie nya.

Data yang akan kita ambil pada proyek ini adalah data API dari TheMovieDB dengan linknya sebagai berikut.
untuk list movie : [https://api.themoviedb.org/3/movie/popular?api_key=eeb7ac3deb1c5ee1471cdd22da14ed1a](https://api.themoviedb.org/3/movie/popular?api_key=eeb7ac3deb1c5ee1471cdd22da14ed1a) 
untuk detail movie : [https://api.themoviedb.org/3/movie/{movie_id}?api_key=eeb7ac3deb1c5ee1471cdd22da14ed1a](https://api.themoviedb.org/3/movie/400160?api_key=eeb7ac3deb1c5ee1471cdd22da14ed1a)
Jika API data JSON nya tidak rapi silakan instal terlebih dahulu extension JSON Formatter pada browser kalian.

Untuk membuat akses data dari API kita membutuhkan beberapa komponen diantaranya DataResponse, ApiConfiguration, ApiInterface, dan ApiListener. 

Proyek ini kita akan melanjutkan proyek sebelumnya, jadi jika belum ada proyek sebelumnya silakan download hasil implementasi Proyek Belajar sebelumnya [disini](https://gitlab.com/RTAgung/poyek-belajar/-/tree/Pertemuan-6).

### Menerapkan API pada List Movie
Pertama kita akan menggantikan data pada list movie terlebih dahulu yang ada di HomeActivity. 

Mari kita mulai implementasinya, ikuti langkah-langkah berikut:
1. Buka Proyek Belajar yang sebelumnya.
2. Sebelum kita memulai menulis code nya, kita perlu menambahkan library dependencies baru untuk membantu pengambilan data dari API, yaitu library Retrofit. Caranya seperti yang pernah kita lakukan yaitu membuka file build.gradle (Module: app), kemudian tambahkan code ini.
    ```gradle
    ...
    
    dependencies {
        ...
        
        implementation 'com.squareup.retrofit2:retrofit:2.9.0'
        implementation 'com.squareup.retrofit2:converter-gson:2.9.0'
    }
    ```
    Setelah itu jangan lupa klik Sync pada bagian atas kanan.
3. Persiapan telah selesai, selanjutnya kita akan mulai membuat beberapa komponen yang dibutuhkan. Pertama kita akan membuat DataResponse, kita akan membuat kelas data model baru dengan nama MovieListResponse, caranya seperti membuat kelas biasa, klik kanan pada package **data** **-->** New **->** Java Class **-->** isi nama dengan **MovieListResponse** **-->** pilih Class **-->** tekan Enter.
4. Selanjutnya buka file `MovieListResponse.java` tersebut dan isi code nya seperti ini.
    ```java
    // membuat kelas untuk menerima nested data object API
    public class MovieListResponse {
        // SerializedName digunakan untuk menyamakan nama variabel di API dengan yang dibuat
        @SerializedName("results")
        private ArrayList<Movie> results;
    
        public MovieListResponse(ArrayList<Movie> results) {
            this.results = results;
        }
    
        public ArrayList<Movie> getResults() {
            return results;
        }
    }
    ```
5. Jika sudah jangan lupa ganti code di `Movie.java` juga, hingga menjadi seperti ini.
    ```java
    // membuat kelas model untuk menampung data data movie
    public class Movie {
        // SerializedName digunakan untuk menyamakan nama variabel di API dengan yang dibuat
        @SerializedName("id")
        private String id;
    
        @SerializedName("title")
        private String title;
    
        @SerializedName("original_title")
        private String originalTitle;
    
        @SerializedName("overview")
        private String overview;
    
        @SerializedName("release_date")
        private String releaseDate;
    
        @SerializedName("backdrop_path")
        private String backdropPath;
    
        @SerializedName("poster_path")
        private String posterPath;
    
        public Movie(String id, String title, String originalTitle, String overview, String releaseDate, String backdropPath, String posterPath) {
            this.id = id;
            this.title = title;
            this.originalTitle = originalTitle;
            this.overview = overview;
            this.releaseDate = releaseDate;
            this.backdropPath = backdropPath;
            this.posterPath = posterPath;
        }
    
        public String getId() {
            return id;
        }
    
        public String getTitle() {
            return title;
        }
    
        public String getOriginalTitle() {
            return originalTitle;
        }
    
        public String getOverview() {
            return overview;
        }
    
        public String getReleaseDate() {
            return releaseDate;
        }
    
        public String getBackdropPath() {
            return backdropPath;
        }
    
        public String getPosterPath() {
            return posterPath;
        }
    }
    ```
6. Jika kita perhatikan 2 code diatas terdapat `@SerializedName(<variable>)` yang merupakan sebagai sinkronisasi nama variabel yang ada di aplikasi dengan variabel yang ada di API bentuk JSON. Maka dari itu penamaan variabel tersebut bukanlah asal-asalan, melainkan harus sama dengan yang ada di API.
7. Setelah kita buat model untuk menampung data, kita akan membuat kelas yang menjadi layanan  API nya, sebelum itu agar struktur lebih rapi kita akan membuat package baru dengan nama **service** di dalam package data, caranya dengan klik kanan pada package data **-->** New **-->** Package **-->** isi nama dengan `com.example.proyekbelajar.data.service` **-->** lalu tekan Enter. 
8. Selanjutnya mari buat tiga kelas baru untuk menjadi layanan akses ke API nya, yaitu kelas MovieApi, MovieApiInterface, MovieListener. Kita akan membuatnya secara bertahap.
9. Pertama kita buat terlebih dahulu kelas **MovieApi** di dalam package service dengan cara klik kanan pada package service **-->** New **-->** Java Class **-->** isi nama dengan **MovieApi** **-->** pilih Class **-->** tekan Enter.
10. Kedua kita buat kelas **MovieApiInterface**, pada kelas ini sedikit berbeda dengan biasanya karena kita akan membuat kelas bentuk interface, caranya hampir sama yaitu klik kanan pada package service **-->** New **-->** Java Class **-->** isi nama dengan **MovieApiInterface** **-->** pilih **Inteface** **-->** tekan Enter.
11. Terakhir kita akan buat kelas **MovieListener**, kelas ini berbentuk interface sama seperti MovieApiListener, caranya yaitu  klik kanan pada package service **-->** New **-->** Java Class **-->** isi nama dengan **MovieListener** **-->** pilih **Inteface** **-->** tekan Enter.
12. Baiklah kita sudah menyediakan 3 kelas untuk dapat mengakses data dari API. Selanjutnya kita akan mengisi code nya satu per satu.
13. Pertama bukalah `MovieApiInterface.java`, kelas ini sebagai tempat pemanggilan fungsi ke link API tujuan, isi codenya menjadi seperti ini.
    ```java
    public interface MovieApiInterface {
    
        // request ambil data dari API dengan link tertentu
        // GET sebagai metode akses yaitu mengambil data
        // Call untuk memanggil object untuk menampung data yang diambil
        // Path untuk membuat link menjadi dinamis
        @GET("/3/movie/popular?api_key=eeb7ac3deb1c5ee1471cdd22da14ed1a")
        Call<MovieListResponse> getListMovie();
    }
    ```
14. Selanjutnya buka file `MovieListener.java`, kelas ini sebagai jempatan pengirim data ke ui/activity, isi codenya seperti berikut.
    ```java
    // membuat interface sebagai callback / jembatan
    // yang mengantarkan data dari pengambilan data (MovieApi) kepada menampilkan data (Activity)
    public interface MovieListener<T> {
        void onSuccess(T body);
        void onFailed(String message);
    }
    ```
15. Terakhir buka file `MovieApi.java`, kelas ini sebagai konfigurasi pengaturan akses API, tambahkan code berikut ini untuk membuat konfigurasi API yang kita butuhkan.
    ```java
    public class MovieApi {
        // membuat variabel untuk menampung URL dari API yang digunakan
        public static final String BASE_URL = "https://api.themoviedb.org/";
    
        // inisialisasi Retrofit sebagai library untuk mengakses API
        private Retrofit retrofit = null;
    
        // fungsi untuk membuat retrofit dan mengkoneksikan dengan request yang ada di MovieApiInterface
        MovieApiInterface getMovieApi() {
            if (retrofit == null) {
                // membangun atau instansiasi data retrofit
                retrofit = new Retrofit
                        .Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit.create(MovieApiInterface.class);
        }
    }
    ```
    Selanjutnya kita tambahkan fungsi method baru untuk eksekusi fungsi link API yang sudah kita buat di MovieApiInterface, tambah codenya seperti berikut.
    ```java
    public class MovieApi {
        ...
        
        // mengambil list movie dari API
        public void getListMovie(MovieListener<ArrayList<Movie>> listener) {
            // eksekusi pemanggilan request ke API
            getMovieApi().getListMovie().enqueue(new Callback<MovieListResponse>() {
                // ketika berhasil mengambil data
                @Override
                public void onResponse(Call<MovieListResponse> call, Response<MovieListResponse> response) {
                    try {
                        // menyimpan hasil pengambilan data
                        MovieListResponse movieResponse = response.body();
                        if (movieResponse != null) {
                            // menyimpan list data dari nested data yang berasal dari API
                            ArrayList<Movie> listMovie = movieResponse.getResults();
                            // mengirimkan data melalui Callback / jembatan listener
                            listener.onSuccess(listMovie);
                        }
                    } catch (Exception e) {
                        // mengirimkan pesan error melalui Callback / jembatan listener
                        listener.onFailed(e.getMessage());
                    }
                }
    
                // ketika gagal atau error pada saat mengambil data
                @Override
                public void onFailure(Call<MovieListResponse> call, Throwable t) {
                    // mengirimkan pesan error melalui Callback / jembatan listener
                    listener.onFailed(t.getMessage());
                }
            });
        }
    }
    ```
    `getListMovie()` digunakan sebagai eksekusi link request API yang ada di MovieApiInterface.
16. Pada tahap ini kita telah menyelesaikan tahap untuk pengambilan data dari API, selanjutnya kita akan menerima datanya di HomeActivity dan ditampilkan dalam bentuk list.
17. Mari kita buka file `HomeActivity.java`, karena tadi kita menerima datanya melewati jembatan MovieListener maka kita akan inisialisasi jembatan/Callback tersebut di kelas `HomeActivity.java`, silakan tambahkan code berikut di `HomeActivity.java`.
    ```java
    public class HomeActivity extends AppCompatActivity {
        ...
        
        // inisialisasi Callback
        private MovieListener<ArrayList<Movie>> listMovieListener = new MovieListener<ArrayList<Movie>>() {
            // menerima data ketika berhasil
            @Override
            public void onSuccess(ArrayList<Movie> body) {
                // memasukkan data ke dalam list
                movieAdapter.setMovies(body);
            }
    
            // menerima data ketika gagal
            @Override
            public void onFailed(String message) {
                // menampilkan notif message
                Toast.makeText(HomeActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        };
        
        ...
    }
    ```
18. Selanjutnya kita tinggal panggil method pemanggilan data API di MovieApi. Pertama kita **hapus** code pemanggilan data sebelumnya yang ini.
    ```java
    public class HomeActivity extends AppCompatActivity {
        ...
        
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            ...
            
            // hapus dari sini
            // memasukkan data ke dalam list
            ArrayList<Movie> data = DataDummy.getDummyMovie();
            movieAdapter.setMovies(data);
            // sampai sini
        }
        
        ...
    }
    ```
    kemudian kita tambahkan cara pemanggilan data yang baru, dengan menggantikan codenya menjadi seperti ini.
    ```java
    public class HomeActivity extends AppCompatActivity {
        ...
        
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            ...
            
            // inisialisasi API
            MovieApi movieApi = new MovieApi();
            // memanggil method getListMovie untuk request data API
            // dengan menggunakan Callback / jembatan listMovieListener
            movieApi.getListMovie(listMovieListener);
        }
        
        ...
    }
    ```
19. Maka full code yang ada di `HomeActivity.java` akan menjadi seperti ini.
    ```java
    public class HomeActivity extends AppCompatActivity {
    
        // deklarasi variabel view sesuai dengan layout xml
        private TextView welcome;
        private RecyclerView rvMovie;
    
        // deklarasi adapter yang telah dibuat
        private MovieAdapter movieAdapter;
    
        // inisialisasi Callback
        private MovieListener<ArrayList<Movie>> listMovieListener = new MovieListener<ArrayList<Movie>>() {
            // menerima data ketika berhasil
            @Override
            public void onSuccess(ArrayList<Movie> body) {
                // memasukkan data ke dalam list
                movieAdapter.setMovies(body);
            }
    
            // menerima data ketika gagal
            @Override
            public void onFailed(String message) {
                // menampilkan notif message
                Toast.makeText(HomeActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        };
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home);
            // mengganti tulisan di action bar (paling atas)
            getSupportActionBar().setTitle("Home");
    
            // koneksikan variabel dengan view berdasarkan id
            welcome = findViewById(R.id.tv_welcome);
            rvMovie = findViewById(R.id.rv_movie_list);
    
            // menerima kiriman data dari halaman sebelumnya menggunakan kuncinya (EXTRA_USERNAME)
            String username = getIntent().getStringExtra(MainActivity.EXTRA_USERNAME);
    
            // menampilkan text ke dalam view
            welcome.setText("Welcome, " + username);
    
            // menyambungkan adapter yang telah dibuat ke dalam variabel
            movieAdapter = new MovieAdapter();
            // memberi jenis leyout yang akan ditampilkan ke view
            rvMovie.setLayoutManager(new LinearLayoutManager(this));
            // memasukkan adapter ke dalam view
            rvMovie.setAdapter(movieAdapter);
    
            // inisialisasi API
            MovieApi movieApi = new MovieApi();
            // memanggil method getListMovie untuk request data API
            // dengan menggunakan Callback / jembatan listMovieListener
            movieApi.getListMovie(listMovieListener);
        }
    
        // memunculkan tombol yang ada di action bar
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // menghubungkan tombol-tombol dengan menu yang telah dibuat sebelumnya
            getMenuInflater().inflate(R.menu.menu_home, menu);
            return super.onCreateOptionsMenu(menu);
        }
    
        // membuat aksi pada tombol yang ada di action bar (paling atas)
        @Override
        public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            // identifikasi tombol mana yang diklik berdasarkan id
            switch (item.getItemId()) {
                // tombol profile
                case R.id.action_profile:
                    // deklarasi intent seperti biasa
                    Intent intent = new Intent(this, ProfileActivity.class);
                    // memulai pindah halaman
                    startActivity(intent);
                    break;
            }
            return super.onOptionsItemSelected(item);
        }
    }
    ```
20. Sampai sini kita sudah berhasil membuat data list movie berasal dari API. Silakan run aplikasinya terlebih dahulu untuk melihat hasil nya. Namun pada detail akan terjadi error karena kita belum membuat pemanggilan data dari API pada DetailActivity.

### Menerapkan API pada Detail Movie
Setelah berhasil membuat data list movie berasal dari API, sekarang kita akan implementasikannya ke dalam detail movie yang ada di DetailActivity.

Disini prosesnya akan lebih singkat karena sebelumnya kita telah membuat komponen service yang dibutuhkan, sekarang kita hanya menambahkan beberapa code saja untuk dapat mengambil data detail dari API.

Mari kita mulai dengan langkah-langkah berikut :
1. Masih di dalam Proyek Belajar, bukalah file `MovieApiInterface.java` lalu tambahkan link fungsi baru untuk mengambil detail data, tambah code nya seperti ini.
    ```java
    public interface MovieApiInterface {
        ...
    
        @GET("/3/movie/{movie_id}?api_key=eeb7ac3deb1c5ee1471cdd22da14ed1a")
        Call<Movie> getDetailMovie(@Path("movie_id") String id);
    }
    ```
2. Sekarang kita telah mempunyai fungsi link API baru, selanjutnya kita tambahkan fungsi method baru untuk eksekusi link tersebut di dalam file `MovieApi.java` dengan nama `getDetailMovie()`.
    ```java
    public class MovieApi {
        ...
    
        // mengambil detail movie dari API berdasarkan id
        public void getDetailMovie(MovieListener<Movie> listener, String id) {
            // eksekusi pemanggilan request ke API
            getMovieApi().getDetailMovie(id).enqueue(new Callback<Movie>() {
                // ketika berhasil mengambil data
                @Override
                public void onResponse(Call<Movie> call, Response<Movie> response) {
                    try {
                        // menyimpan hasil pengambilan data
                        Movie movie = response.body();
                        if (movie != null) {
                            // mengirimkan data melalui Callback / jembatan listener
                            listener.onSuccess(movie);
                        }
                    } catch (Exception e) {
                        // mengirimkan pesan error melalui Callback / jembatan listener
                        listener.onFailed(e.getMessage());
                    }
                }
    
                // ketika gagal atau error pada saat mengambil data
                @Override
                public void onFailure(Call<Movie> call, Throwable t) {
                    // mengirimkan pesan error melalui Callback / jembatan listener
                    listener.onFailed(t.getMessage());
                }
            });
        }
    }
    ```
3. Langkah terakhir kita tinggal modifikasi isi file `DetailActivity.java` seperti pada file `HomeActivity.java`, yaitu membuat inisialisasi jembatan/Callback yang akan digunakan dan pemanggilan data. Pertama kita tambahkan dulu Callback yang digunakan pada file `DetailActivity.java`.
    ```java
    public class DetailActivity extends AppCompatActivity {
        ...
    
        // inisialisasi Callback
        private MovieListener<Movie> detailMovieListener = new MovieListener<Movie>() {
            // menerima data ketika berhasil
            @Override
            public void onSuccess(Movie body) {
                // memanggil method untuk menampilkan data ke dalam view (tampilan)
                // dengan parameter data hasil dari request API
                populateView(body);
            }
    
            // menerima data ketika gagal
            @Override
            public void onFailed(String message) {
                // menampilkan notif message
                Toast.makeText(DetailActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        };
    
        ...
    }
    ```
4. Kemudian kita modifikasi cara pemanggilan datanya. Sama seperti proses pada `HomeActivity.java` kita **hapus** terlebih dahulu cara pemanggilan data yang lama di `DetailActivity.java`, seperti berikut.
    ```java
    public class DetailActivity extends AppCompatActivity {
        ...
        
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            ...
    
            // hapus dari sini
            // mengambil data dari data sementara (data dummy) berdasarkan id
            Movie movie = DataDummy.getDummyDetailMovie(id);
    
            // memanggil method untuk menampilkan data ke dalam view (tampilan)
            populateView(movie);
            // hapus sampai sini
        }
    
        ...
    }
    ```
    Kemudian kita tambahkan code cara pemanggilan data baru menggunakan API, tambahkan code berikut.
    ```java
    public class DetailActivity extends AppCompatActivity {
        ...
        
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            ...
    
            // inisialisasi API
            MovieApi movieApi = new MovieApi();
            // memanggil method getDetailMovie untuk request data API
            // dengan menggunakan Callback / jembatan detailMovieListener
            // dan mengirimkan id yang spesifik
            movieApi.getDetailMovie(detailMovieListener, id);
        }
    
        ...
    }
    ```
5. Maka full code yang ada di `DetailActivity.java` akan menjadi seperti ini.
    ```java
    public class DetailActivity extends AppCompatActivity {
    
        // deklarasi kunci / key untuk pengiriman data id
        public static final String EXTRA_ID = "extra_id";
    
        // membuat variabel konstanta untuk melengkapi link yang kurang
        private static final String IMAGE_URL = "https://image.tmdb.org/t/p/w500/";
    
        // deklarasi variabel view sesuai dengan layout xml
        private TextView tvTitle, tvOriginalTitle, tvReleaseDate, tvOverview;
        private ImageView ivPoster;
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_detail);
            // mengganti tulisan di action bar (paling atas)
            getSupportActionBar().setTitle("Detail");
            // memunculkan tombol back di action bar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    
            // menerima id yang dikirim dari HomeActivity
            String id = getIntent().getStringExtra(EXTRA_ID);
    
            // koneksikan variabel dengan view berdasarkan id
            tvTitle = findViewById(R.id.tv_detail_title);
            tvOriginalTitle = findViewById(R.id.tv_detail_original_title);
            tvReleaseDate = findViewById(R.id.tv_detail_release);
            tvOverview = findViewById(R.id.tv_detail_overview_text);
            ivPoster = findViewById(R.id.iv_detail_poster);
    
            // mengambil data dari data sementara (data dummy) berdasarkan id
            Movie movie = DataDummy.getDummyDetailMovie(id);
    
            // memanggil method untuk menampilkan data ke dalam view (tampilan)
            populateView(movie);
        }
    
        // membuat aksi pada tombol yang ada di action bar (paling atas)
        @Override
        public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            // identifikasi tombol mana yang diklik berdasarkan id
            switch (item.getItemId()) {
                // tombol back pada action bar
                case android.R.id.home:
                    finish();
                    break;
            }
            return super.onOptionsItemSelected(item);
        }
    
        // untuk menampilkan data ke dalam view (tampilan)
        private void populateView(Movie movie) {
            // menampilkan text ke dalam view item
            tvTitle.setText(movie.getTitle());
            tvOriginalTitle.setText(movie.getOriginalTitle());
            tvReleaseDate.setText("Release on " + movie.getReleaseDate());
            tvOverview.setText(movie.getOverview());
    
            // mengambil dan menampilkan image berdasarkan link ke dalam view item
            Glide.with(this)
                    .load(IMAGE_URL + movie.getPosterPath())
                    .into(ivPoster);
        }
    }
    ```
6. Apabila telah selesai, maka aplikasi sudah dapat kita jalankan seperti semula namun dengan data yang lebih dinamis karena berasal dari server.

Selamat! Kita telah menempuh banyak rintangan dan akhirnya kita dapat membuat proyek dengan lebih luas menggunakan data dari server. Dengan ini kalian telah dapat membuat proyek baru yang lebih kreatif dan inovatif. Semoga materi yang telah diberikan selama ini bermanfaat untuk kedepannya.

source code dapat diakses pada link berikut :
[Menggunakan API Pada Proyek Belajar](https://gitlab.com/RTAgung/poyek-belajar/-/tree/Pertemuan-7)
