# Implementasi View pada Proyek Belajar

Mari kita mulai untuk membuat proyek belajar ini secara bertahap, ikuti langkah-langkah berikut ini :
1. Buatlah project baru dengan nama **Proyek Belajar** dengan cara :
    klik New Project **-->** Pilih Empty Activity **-->** isi nama dengan Proyek Belajar **-->** ubah Save Location sesuka kalian **-->** Language pilih Java **-->** untuk minimum SDK tidak perlu diubah **-->** terakhir klik Finish.
2. Selanjutnya sebelum kita memulai bikin code nya, kita akan mempersiapkan asset yang akan digunakan sampai akhir proyek ini, silakan download terlebih dahulu asset image yang akan dibutuhkan [disini](https://drive.google.com/drive/folders/1iVj-Z6SZ6wA6sMGj_UrzWJaqoruOw5sY?usp=sharing).
3. Setelah selesai download silakan copy semua file image yang ada ke dalam proyek belajar kita, caranya dengan copy langsung semua file lalu paste ke dalam folder drawable, nanti akan ada dialog maka langsung klik OK saja.
4. Jika sudah, selanjutnya kita mulai implementasikan View terhadap project kita, pertama kita buka terlebih dahulu file `activity_main.xml` yang ada di folder res > layout.
5. Kita akan membuat tampilan login seperti pada umumnya dengan menggunakan RelativeLayout, ImageView, TextInputLayout, LinearLayout, dan Button.
6. Silakan coba code dibawah ini dan jalankan ke perangkat kalian.
    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:gravity="center"
        android:padding="16dp"
        tools:context=".MainActivity">
    
        <ImageView
            android:id="@+id/iv_image"
            android:layout_width="300dp"
            android:layout_height="250dp"
            android:layout_centerHorizontal="true"
            android:layout_margin="16dp"
            android:src="@drawable/login" />
    
        <com.google.android.material.textfield.TextInputLayout
            android:id="@+id/layout_username"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_below="@id/iv_image"
            android:layout_margin="16dp">
    
            <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/input_username"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:hint="Username"
                android:inputType="text" />
        </com.google.android.material.textfield.TextInputLayout>
    
        <com.google.android.material.textfield.TextInputLayout
            android:id="@+id/layout_password"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_below="@id/layout_username"
            android:layout_margin="16dp">
    
            <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/input_password"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:hint="Password"
                android:inputType="textPassword" />
        </com.google.android.material.textfield.TextInputLayout>
    
        <LinearLayout
            android:id="@+id/layout_button"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_below="@id/layout_password"
            android:orientation="horizontal">
    
            <Button
                android:id="@+id/btn_login"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_margin="16dp"
                android:layout_weight="1"
                android:text="Login" />
    
            <Button
                android:id="@+id/btn_signup"
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_margin="16dp"
                android:layout_weight="1"
                android:text="Signup" />
        </LinearLayout>
    </RelativeLayout>
    ```
7. Seperti yang terlihat pada code diatas, kita menggunakan ViewGroup di dalam ViewGroup yaitu LinearLayout di dalam RelativeLayout. RelativeLayout kita gunakan sebagai parent yang menyediakan layout utama, sedangkan LinearLayout kita gunakan untuk menyesuaikan letak Button agar rapi.
8. Lalu kita menggunakan TextInputLayout dan TextInputEditText untuk menyediakan EditText dengan tampilan yang lebih bagus.

Selamat! kita sudah berhasil membuat tampilan login.

Untuk source code dari implementasi view ini dapat dilihat disini : 
[Proyek Belajar - Implementasi View](https://gitlab.com/RTAgung/poyek-belajar/-/tree/Pertemuan-2)