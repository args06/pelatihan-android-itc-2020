# Integrasi View Pada Proyek Belajar

Sebelumnya kita telah membuat tampilan login yang rapi, namun kita belum dapat berinteraksi terhadap tampilan tersebut. Pada implementasi kali ini kita akan membuat fungsi dari tiap tiap View yang digunakan seperti Button dan EditText nya, kita akan membuat fungsi login sungguhan namun tidak berpindah halaman activity.

Proyek ini kita akan melanjutkan proyek sebelumnya, jadi jika belum ada proyek sebelumnya silakan download hasil implementasi Proyek Belajar sebelumnya [disini](https://gitlab.com/RTAgung/poyek-belajar/-/tree/Pertemuan-2).

Mari kita mulai implementasinya, ikuti langkah-langkah berikut:
1. Buka Proyek Belajar yang sebelumnya
2. Lihat view yang ada pada `activity_main.xml` dan pastikan sudah ada atribut id pada tiap view tersebut.
3. Jika sudah memiliki atribut id pada view yang akan dimanipulasi, selanjutnya buka file kelas `MainActivity.java`.
4. Kita akan memulai membuat code pada kelas MainActivity.
5. Buatlah deklarasi viewnya terlebih dahulu dan hubungkan varibel tersebut dengan view yang ada di `activity_main.xml`.
    ```java
    public class MainActivity extends AppCompatActivity {
    
        // deklarasi variabel view sesuai dengan layout xml
        private TextInputEditText inputUsername, inputPassword;
        private Button btnLogin, btnSignup;
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
    
            // koneksikan variabel dengan view berdasarkan id
            inputUsername = findViewById(R.id.input_username);
            inputPassword = findViewById(R.id.input_password);
            btnLogin = findViewById(R.id.btn_login);
            btnSignup = findViewById(R.id.btn_signup);
        }
    }
    ```
6. Selanjutnya kita akan membuat Button yang ada di view mempunyai aksi, tambahkan code berikut.
    ```java
    public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    
        // deklarasi variabel view sesuai dengan layout xml
        private TextInputEditText inputUsername, inputPassword;
        private Button btnLogin, btnSignup;
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
    
            ...
    
            // inisialisasi fungsi klik pada tombol
            btnLogin.setOnClickListener(this);
            btnSignup.setOnClickListener(this);
        }
    
        @SuppressLint("NonConstantResourceId")
        @Override
        public void onClick(View v) {
            
        }
    }
    ```
    Perhatikan bahwa kelas MainActivity sekarang `implements View.OnClickListener` untuk dapat membuat fungsi aksi pada tombol.
7. Kemudian kita tambahkan fungsi aksi apabila tombol tersebut diklik. Tambah code seperti berikut.
    ```java
    public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    
        ...
        
        @SuppressLint("NonConstantResourceId")
        @Override
        public void onClick(View v) {
            // identifikasi tombol mana yang diklik berdasarkan id
            switch (v.getId()) {
                case R.id.btn_login:
                    login();
                    break;
                case R.id.btn_signup:
                    signup();
                    break;
            }
        }
    
        private void signup() {
            // menampilkan notif
            Toast.makeText(this, "you have been registered", Toast.LENGTH_SHORT).show();
        }
    
        private void login() {
            // mengambil nilai string pada text input di view
            String username = inputUsername.getText().toString();
            String password = inputPassword.getText().toString();
    
            // memeriksa apakah inputnya kosong atau tidak
            // jika kosong diberikan error "username couldn't be empty"
            if (username.isEmpty()) inputUsername.setError("username couldn't be empty");
            else if (password.isEmpty()) inputPassword.setError("password couldn't be empty");
            else {
                // melakukan pemeriksaan login
                if (username.equals("admin") && password.equals("admin123")) {
                    Toast.makeText(this, "login success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "login failed", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    ```
    pada `void login()` kita mengambil nilai teks yang ada di EditText view dan memeriksa isinya apakah **username** sama dengan **admin** dan **password** sama dengan **admin123**, jika ya maka muncul notif berhasil login, jika tidak maka muncul notif gagal login.
8. Sehingga code seluruh akan seperti ini.
    ```java
    public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    
        // deklarasi variabel view sesuai dengan layout xml
        private TextInputEditText inputUsername, inputPassword;
        private Button btnLogin, btnSignup;
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
    
            // koneksikan variabel dengan view berdasarkan id
            inputUsername = findViewById(R.id.input_username);
            inputPassword = findViewById(R.id.input_password);
            btnLogin = findViewById(R.id.btn_login);
            btnSignup = findViewById(R.id.btn_signup);
    
            // inisialisasi fungsi klik pada tombol
            btnLogin.setOnClickListener(this);
            btnSignup.setOnClickListener(this);
        }
    
        @SuppressLint("NonConstantResourceId")
        @Override
        public void onClick(View v) {
            // identifikasi tombol mana yang diklik berdasarkan id
            switch (v.getId()) {
                case R.id.btn_login:
                    login();
                    break;
                case R.id.btn_signup:
                    signup();
                    break;
            }
        }
    
        private void signup() {
            // menampilkan notif
            Toast.makeText(this, "you have been registered", Toast.LENGTH_SHORT).show();
        }
    
        private void login() {
            // mengambil nilai string pada text input di view
            String username = inputUsername.getText().toString();
            String password = inputPassword.getText().toString();
    
            // memeriksa apakah inputnya kosong atau tidak
            // jika kosong diberikan error "username couldn't be empty"
            if (username.isEmpty()) inputUsername.setError("username couldn't be empty");
            else if (password.isEmpty()) inputPassword.setError("password couldn't be empty");
            else {
                // melakukan pemeriksaan login
                if (username.equals("admin") && password.equals("admin123")) {
                    Toast.makeText(this, "login success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "login failed", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    ```
9. Silakan coba dan jalankan pada perangkat.

Selamat! sekarang kita dapat membuat aksi pada view yang kita buat. Besok kita akan membuat login berhasil dengan berpindah halaman activity.

source code dapat diakses pada link berikut :
[Integrasi View Pada Proyek Belajar](https://gitlab.com/RTAgung/poyek-belajar/-/tree/Pertemuan-3)
