# Recycler View Pada Proyek Belajar

Dari Proyek Belajar sebelumnya kita berhasil menampilkan list movie dengan cukup banyak, namun masih bersifat statis dengan menambahkan view langsung didalam layout nya. Kali ini kita akan implementasikan Recycler View sehingga layout yang kita gunakan tidak perlu menambah view lagi apabila data nya bertambah.

Untuk dapat mengimplementasikan Recycler View kita butuh komponen tambahan yang baru, seperti Adapter, Data, Model, Recycler View sendiri.

Proyek ini kita akan melanjutkan proyek sebelumnya, jadi jika belum ada proyek sebelumnya silakan download hasil implementasi Proyek Belajar sebelumnya [disini](https://gitlab.com/RTAgung/poyek-belajar/-/tree/Pertemuan-4).

Mari kita mulai implementasinya, ikuti langkah-langkah berikut:
1. Buka Proyek Belajar yang sebelumnya
2. Disini kita akan menggunakan Recycler View dan gambar tiap movie akan diambil melalui internet, maka dari itu kita perlu menambahkan library dependencies baru pada gradle. Caranya buka file build.gradle (Module: .app) yang berada di folder gradle dibawah folder App.
3. jika telah dibuka file build.gradle, silakan tambah code nya menjadi seperti berikut.
    ```gradle
    plugins {
        ...
    }
    
    android {
        ...
    }
    
    dependencies {
    
        ...
    
        implementation 'androidx.recyclerview:recyclerview:1.2.0-beta01'
        implementation 'com.github.bumptech.glide:glide:4.11.0'
    }
    ```
4. Apabila telah menambahkan dependencies jangan lupa untuk klik **Sync** pada bagian atas kanan.
5. Selanjutnya kita akan memberikan akses internet terhadap aplikasi karena gambar yang akan kita tampilkan bersumber dari internet. Caranya buka file `AndroidManifest.xml` yang berada di folder manifest (bagian atas), kemudian tambah `user-permission` hingga code nya menjadi seperti ini.
    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <manifest xmlns:android="http://schemas.android.com/apk/res/android"
        package="com.example.proyekbelajar">
    
        <uses-permission android:name="android.permission.INTERNET" />
    
        ...
    
    </manifest>
    ```
6. Persiapan telah kita laksanakan, selanjutnya kita akan langsung memulai membuat recycler view. Pertama buka file `activity_home.xml` di folder res **-->** layout. Ubah code nya menjadi seperti ini.
    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        tools:context=".ui.HomeActivity">
    
        <TextView
            android:id="@+id/tv_welcome"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:gravity="center_horizontal"
            android:text="Welcome"
            android:textSize="24sp"
            android:textStyle="bold" />
    
        <androidx.recyclerview.widget.RecyclerView
            android:id="@+id/rv_movie_list"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            tools:listitem="@layout/movie_item" />
    </LinearLayout>
    ```
7. Code diatas akan terjadi error karena kita membutuhkan layout tambahan baru. Buatlah layout baru dengan cara klik kanan folder layout **-->** New **-->** Layout Resource File **-->** isi file name dengan **movie_item** **-->** klik OK.
8. Isi code di dalam `movie_item.xml` menjadi seperti ini.
    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">
    
        <RelativeLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_margin="16dp"
            android:background="#BBDEFB">
    
            <ImageView
                android:id="@+id/iv_item_image"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:adjustViewBounds="true"
                android:src="@drawable/movie" />
    
            <TextView
                android:id="@+id/tv_item_title"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_below="@id/iv_item_image"
                android:gravity="center_horizontal"
                android:text="Spongebob"
                android:textColor="@color/black"
                android:textSize="18sp"
                android:textStyle="bold" />
        </RelativeLayout>
    </LinearLayout>
    ```
9. Pada tahap ini kita telah membuat tampilannya, selanjutnya kita akan mempersiapkan pada sisi logic nya.
10. Seperti yang dibutuhkan dalam membuat Recycler View, kita harus menyiapkan Data, Model, dan Adapter. Kita akan membuat ketiga kelas tersebut satu per satu. Namun sebelum itu kita akan merapikan folder package agar mudah dilihat.
11. Buatlah folder package data dengan cara berikut. Pada folder java klik kanan di folder/package **proyekbelajar** **-->** New **-->** Package **-->** isi kolom dengan `com.example.proyekbelajar.data` **-->** lalu tekan Enter.
12. Membuat package ui. Pada folder java klik kanan di folder/package **proyekbelajar** **-->** New **-->** Package **-->** isi kolom dengan `com.example.proyekbelajar.ui` **-->** lalu tekan Enter.
13. Membuat package utils. Pada folder java klik kanan di folder/package **proyekbelajar** **-->** New **-->** Package **-->** isi kolom dengan `com.example.proyekbelajar.utils` **-->** lalu tekan Enter.
14. Membuat package adapter didalam package ui. Pada folder java klik kanan di folder/package **ui** **-->** New **-->** Package **-->** isi kolom dengan `com.example.proyekbelajar.ui.adapter` **-->** lalu tekan Enter.
15. Setelah selesai membuat package-package tersebut, kita akan pindahkan file Activity yang ada ke dalam package yang sesuai. Caranya blok ketiga file (**MainActivity, HomeActivity, ProfileActivity**) lalu geser ke dalam folder/package **ui**, nanti akan muncul dialog, pada dialog tersebut klik Refactor. Sehingga file tersebut akan dipindahkan ke dalam package **ui** apabila di package ui tidak ter-refresh, maka bisa restart android studio nya terlebih dahulu.
16. Jika sudah semua, kita akan mulai membuat Data, Model, dan Adapter.
17. Pertama kita buat model data terlebih dahulu di dalam package data dengan nama **Movie**, dengan cara klik kanan package data **-->** New **-->** Java Class **-->** isi nama Movie **-->** pilih Class **-->** tekan Enter.
18. Kemudian isi code yang ada di `Movie.class` menjadi seperti ini.
    ```java
    // membuat kelas model untuk menampung data data movie
    public class Movie {
        private String id;
        private String title;
        private String backdropPath;
    
        public Movie(String id, String title, String backdropPath) {
            this.id = id;
            this.title = title;
            this.backdropPath = backdropPath;
        }
    
        public String getId() {
            return id;
        }
    
        public String getTitle() {
            return title;
        }
    
        public String getBackdropPath() {
            return backdropPath;
        }
    }
    ```
19. Selanjutnya kita akan membuat kelas DataDummy di dalam package utils, caranya klik kanan package utils **-->** New **-->** Java Class **-->** isi nama DataDummy **-->** pilih Class **-->** tekan Enter.
20. Isi code di `DataDummy.class` seperti berikut.
    ```java
    public class DataDummy {
        // mengisi data sementara
        public static ArrayList<Movie> dummyMovie() {
            ArrayList<Movie> listMovie = new ArrayList<>();
    
            listMovie.add(
                    new Movie(
                            "590706",
                            "Jiu Jitsu",
                            "/jeAQdDX9nguP6YOX6QSWKDPkbBo.jpg"
                    )
            );
            listMovie.add(
                    new Movie(
                            "602211",
                            "Fatman",
                            "/ckfwfLkl0CkafTasoRw5FILhZAS.jpg"
                    )
            );
            listMovie.add(
                    new Movie(
                            "671583",
                            "Upside-Down Magic",
                            "/vam9VHLZl8tqNwkp1zjEAxIOrkk.jpg"
                    )
            );
            listMovie.add(
                    new Movie(
                            "590706",
                            "Jiu Jitsu",
                            "/jeAQdDX9nguP6YOX6QSWKDPkbBo.jpg"
                    )
            );
            listMovie.add(
                    new Movie(
                            "602211",
                            "Fatman",
                            "/ckfwfLkl0CkafTasoRw5FILhZAS.jpg"
                    )
            );
            listMovie.add(
                    new Movie(
                            "671583",
                            "Upside-Down Magic",
                            "/vam9VHLZl8tqNwkp1zjEAxIOrkk.jpg"
                    )
            );
    
            return listMovie;
        }
    }
    ```
21. Yang terakhir kita membuat MovieAdapter di package adapter, dengan cara klik kanan package adapter **-->** New **-->** Java Class **-->** isi nama MovieAdapter **-->** pilih Class **-->** tekan Enter.
22. Isi code `MovieAdapter.class` menjadi seperti ini
    ```java
    // membuat kelas adapter sebagai penghubung antara list dan item nya
    public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder> {
        // deklarasi list yang akan ditampilkan
        private ArrayList<Movie> movies = new ArrayList<>();
    
        // mengisi data list yang akan di tampilkan
        public void setMovies(ArrayList<Movie> movies) {
            if (movies != null) {
                this.movies.clear();
                this.movies.addAll(movies);
            }
            // update tampilan jika terdapat perubahan data
            notifyDataSetChanged();
        }
    
        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            // mengkoneksikan layout item kedalam kelas adapter ini
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
            return new ViewHolder(view);
        }
    
        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            // mengirimkan data tiap satuan item ke kelas ViewHolder untuk ditampilkan
            holder.bind(movies.get(position));
        }
    
        // mengambil panjang list yang ada
        @Override
        public int getItemCount() {
            return movies.size();
        }
    
        // membuat kelas sebagai kontrol item di dalam view (movie_item.xml) agar dapat dimanipulasi
        public class ViewHolder extends RecyclerView.ViewHolder {
            // membuat variabel konstanta untuk melengkapi link yang kurang
            private static final String IMAGE_URL = "https://image.tmdb.org/t/p/w500/";
    
            // deklarasi variabel view sesuai dengan layout xml
            private TextView tvTitle;
            private ImageView ivImage;
    
            public ViewHolder(@NonNull View itemView) {
                super(itemView);
                // koneksikan variabel dengan view berdasarkan id
                tvTitle = itemView.findViewById(R.id.tv_item_title);
                ivImage = itemView.findViewById(R.id.iv_item_image);
            }
    
            public void bind(Movie movie) {
                // menampilkan text ke dalam view item
                tvTitle.setText(movie.getTitle());
                // mengambil dan menampilkan image berdasarkan link ke dalam view item
                Glide.with(itemView.getContext())
                        .load(IMAGE_URL + movie.getBackdropPath())
                        .into(ivImage);
            }
        }
    }
    ```
23. Semua komponen telah kita buat, sekarang tinggal kita modifikasi code yang ada di `HomeActivity.class` untuk memanggil semua komponen tersebut. Tambahkan code berikut di `HomeActivity.class`.
    ```java
    public class HomeActivity extends AppCompatActivity {
    
        ... 
        
        // deklarasi variabel view sesuai dengan layout xml
        private RecyclerView rvMovie;
    
        // deklarasi adapter yang telah dibuat
        private MovieAdapter movieAdapter;
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home);
            // mengganti tulisan di action bar (paling atas)
            getSupportActionBar().setTitle("Home");
    
            ...
    
            // koneksikan variabel dengan view berdasarkan id
            rvMovie = findViewById(R.id.rv_movie_list);
    
            // menyambungkan adapter yang telah dibuat ke dalam variabel
            movieAdapter = new MovieAdapter();
            // memberi jenis leyout yang akan ditampilkan ke view
            rvMovie.setLayoutManager(new LinearLayoutManager(this));
            // memasukkan adapter ke dalam view
            rvMovie.setAdapter(movieAdapter);
    
            // memasukkan data ke dalam list
            ArrayList<Movie> data = DataDummy.dummyMovie();
            movieAdapter.setMovies(data);
        }
        
        ...
        
    }
    ```
24. Sehingga code di `HomeActivity.class` akan menjadi seperti berikut.
    ```java
    public class HomeActivity extends AppCompatActivity {
    
        // deklarasi variabel view sesuai dengan layout xml
        private TextView welcome;
        private RecyclerView rvMovie;
    
        // deklarasi adapter yang telah dibuat
        private MovieAdapter movieAdapter;
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_home);
            // mengganti tulisan di action bar (paling atas)
            getSupportActionBar().setTitle("Home");
    
            // koneksikan variabel dengan view berdasarkan id
            welcome = findViewById(R.id.tv_welcome);
            rvMovie = findViewById(R.id.rv_movie_list);
    
            // menerima kiriman data dari halaman sebelumnya menggunakan kuncinya (EXTRA_USERNAME)
            String username = getIntent().getStringExtra(MainActivity.EXTRA_USERNAME);
    
            // menampilkan text ke dalam view
            welcome.setText("Welcome, " + username);
    
            // menyambungkan adapter yang telah dibuat ke dalam variabel
            movieAdapter = new MovieAdapter();
            // memberi jenis leyout yang akan ditampilkan ke view
            rvMovie.setLayoutManager(new LinearLayoutManager(this));
            // memasukkan adapter ke dalam view
            rvMovie.setAdapter(movieAdapter);
    
            // memasukkan data ke dalam list
            ArrayList<Movie> data = DataDummy.dummyMovie();
            movieAdapter.setMovies(data);
        }
    
        // memunculkan tombol yang ada di action bar
        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // menghubungkan tombol-tombol dengan menu yang telah dibuat sebelumnya
            getMenuInflater().inflate(R.menu.menu_home, menu);
            return super.onCreateOptionsMenu(menu);
        }
    
        // membuat aksi pada tombol yang ada di action bar (paling atas)
        @Override
        public boolean onOptionsItemSelected(@NonNull MenuItem item) {
            // identifikasi tombol mana yang diklik berdasarkan id
            switch (item.getItemId()) {
                // tombol profile
                case R.id.action_profile:
                    // deklarasi intent seperti biasa
                    Intent intent = new Intent(this, ProfileActivity.class);
                    // memulai pindah halaman
                    startActivity(intent);
                    break;
            }
            return super.onOptionsItemSelected(item);
        }
    }
    ```
25. Sampai tahap ini aplikasi sudah bisa dijalankan dengan menerapkan Recycler View.
26. Untuk strukturnya filenya akan menjadi seperti ini.
    ![struktur](https://cdn.discordapp.com/attachments/726429998654947400/806048972095356928/unknown.png)

Selamat! kita telah berhasil mengubah list data menjadi lebih dinamis dengan akses gambar dari internet. Pada pertemuan selanjutnya kita akan membuat aksi pada setiap list item yang ada.

source code dapat diakses pada link berikut :
[Recycler View Pada Proyek Belajar](https://gitlab.com/RTAgung/poyek-belajar/-/tree/Pertemuan-5)
