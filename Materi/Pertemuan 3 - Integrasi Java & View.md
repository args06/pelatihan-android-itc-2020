### NestedView
Nah, kemarin kita sudah belajar banyak tipe-tipe View dan ViewGroup. Seperti yang dijelaskan di pertemuan sebelumnya, 
>ViewGroup adalah sebuah obyek yang mewadahi obyek-obyek View dan ViewGroup itu sendiri sehingga membentuk satu kesatuan tampilan aplikasi yang utuh.

Jadi, kita dapat menempatkan ViewGroup di dalam sebuah viewgroup itu sendiri. Jenis ini dapat kita namakan sebagai NestedView atau view bertingkat. Untuk komponen View dan ViewGroup dapat digambarkan melalui diagram berikut ini :

![Diagram](https://gitlab.com/args06/pelatihan-android-itc-2020/-/raw/master/images/hierarki.png)

Agar lebih paham dalam penggunaan NestedView, anda dapat melihatnya di contoh kodingan berikut ini :
```xml
<?xml version="1.0" encoding="utf-8"?>
<!-- Ini ViewGroup luar -->
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical" >

    <!-- Ini View Luar -->
    <TextView android:id="@+id/text"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="This is TextView" />

    <!-- Ini View Luar -->
    <Button android:id="@+id/button" 
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="This is Button" />

    <!-- Ini ViewGroup dalam -->
    <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android" 
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="horizontal">

        <!-- Ini View dalam -->
        <TextView android:id="@+id/text2"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="This is TextView" />

    </LinearLayout>
</LinearLayout>
```

### Integrasi Java dengan View
Sebelum memahami tentang integrasi java dan view maka kita perlu membuat sebuah project android.

**Membuat Project Android**
1.	Pada jendela Welcome to Android Studio, klik Start a new Android Studio project. Jika sudah membuka project, pilih File > New > New Project.
2.	Di jendela Select a Project Template, pilih Empty Activity lalu klik Next.
3.	Di jendela Configure your project, lakukan langkah-langkah berikut:
    -	Masukkan "My First App" dalam kolom Name.
    -	Masukkan "com.example.ITC" pada kolom Package name.
    -	Jika ingin menempatkan project di folder lain, ubah lokasi Save-nya.
    -	Pilih Java dari menu drop-down Language.
    -	Pilih versi terendah Android yang akan didukung aplikasi Anda di kolom Minimum SDK (Recommend, sdk 20).
    -	Jika aplikasi Anda memerlukan dukungan library lama, tandai kotak centang Use legacy android.support libraries.
    -	Biarkan opsi lain sebagaimana adanya.
4.	Klik Finish.

**Integrasi Java dan View**

Integrasi java dan view diperlukan agar tampilan view pada aplikasi dapat dihubungkan dengan java sehingga kita dapat mengatur view melalui java. 

Untuk mengintegrasi java dan view pertama kita perlu menghubungkan java dengan viewgroupnya dengan cara :

XML File (`activity_main.xml`)
```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
        android:id="@+id/textMain"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Hello World!" />

</LinearLayout>
```
Java Class (`MainActivity.class`)
```java
package com.example.myapplication;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);//view.xml yang ingin dihubungkan.
    }
}
```
Setelah viewgroup terhubung sekarang kita hanya perlu menghubungkan view dengan java. Dengan cara ```findViewById```.
```java
package com.example.myapplication;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView favText;//mendeklarasikan TextView.
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);//view.xml yang ingin dihubungkan.
        favText = findViewById(R.id.textMain);//menyimpan id view Textview.
    }
}
```

### Proyek Membuat Music Player Sederhana
Agar pembaca lebih paham, kita akan membuat Media Player sederhana. Berikut cara pembuatan Media Player sederhana.

1. Buat Project Baru Empty Activity.
2. Tambahkan gambar yang ingin digunakan dengan cara mengcopy gambar ke directory app > res > drawable lalu masukan nama gambar.
    > Note : Untuk penambahan gambar diwajibkan menggunakan huruf lowercase.
3. Tambahkan vector dengan cara klik kanan pada drawable > new > Vector asset. Pilih asset yang akan digunakan untuk aplikasi.
4. Ubah activity_main.xml menjadi seperti berikut : 

    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        tools:context=".MainActivity">
    
        <ImageView
            android:layout_width="300dp"
            android:layout_height="300dp"
            android:layout_gravity="center"
            android:layout_marginTop="80dp"
            android:src="@drawable/gambar" />
    
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="40dp"
            android:text="Music"
            android:textAlignment="center"
            android:textSize="40sp" />
    
        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_marginTop="30dp"
            android:gravity="center_horizontal">
    
            <ImageButton
                android:layout_width="80dp"
                android:layout_height="80dp"
                android:layout_weight="1"
                android:background="@null"
                android:scaleType="fitCenter"
                android:src="@drawable/ic_baseline_arrow_back_ios_24" />
    
            <ImageButton
                android:layout_width="80dp"
                android:layout_height="80dp"
                android:layout_weight="1"
                android:background="@null"
                android:scaleType="fitCenter"
                android:src="@drawable/ic_baseline_play_arrow_24" />
    
            <ImageButton
                android:layout_width="80dp"
                android:layout_height="80dp"
                android:layout_weight="1"
                android:background="@null"
                android:scaleType="fitCenter"
                android:src="@drawable/ic_baseline_arrow_forward_ios_24" />
        </LinearLayout>
    
    </LinearLayout>
    ```
5. Setelah selesai, tambahkan id pada view seperti berikut.

    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        tools:context=".MainActivity">
    
        <ImageView
            android:layout_width="300dp"
            android:layout_height="300dp"
            android:layout_gravity="center"
            android:layout_marginTop="80dp"
            android:src="@drawable/gambar" />
    
        <TextView
            android:id="@+id/tv_namaMusic"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="40dp"
            android:text="Music"
            android:textAlignment="center"
            android:textSize="40sp" />
    
        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_marginTop="30dp"
            android:gravity="center_horizontal">
    
            <ImageButton
                android:id="@+id/ib_backButton"
                android:layout_width="80dp"
                android:layout_height="80dp"
                android:layout_weight="1"
                android:background="@null"
                android:scaleType="fitCenter"
                android:src="@drawable/ic_baseline_arrow_back_ios_24" />
    
            <ImageButton
                android:id="@+id/ib_playButton"
                android:layout_width="80dp"
                android:layout_height="80dp"
                android:layout_weight="1"
                android:background="@null"
                android:scaleType="fitCenter"
                android:src="@drawable/ic_baseline_play_arrow_24" />
    
            <ImageButton
                android:id="@+id/ib_nextButton"
                android:layout_width="80dp"
                android:layout_height="80dp"
                android:layout_weight="1"
                android:background="@null"
                android:scaleType="fitCenter"
                android:src="@drawable/ic_baseline_arrow_forward_ios_24" />
        </LinearLayout>
    
    </LinearLayout>
    ```

    > Note : ID yang digunakan untuk setiap view harus bersifat unik, karena akan digunakan  mendapatkan referensi yang akan digunakan di dalam java.

6. Langkah selanjutnya yaitu menyambungkan view dari file xml ke java dan menambahkan beberapa variable dengan mengubah file MainActivity menjadi : 

    ```java
    package com.example.itc;
    
    import androidx.appcompat.app.AppCompatActivity;
    
    import android.media.Image;
    import android.os.Bundle;
    import android.widget.ImageButton;
    import android.widget.TextView;
    
    public class MainActivity extends AppCompatActivity {
    
        private String[] songName = {"Lagu 1", "Lagu 2", "Lagu 3"};
        private int trackSong = 0;
        private boolean playing = false;
        TextView tv_namaMusic;
        ImageButton ib_playButton, ib_backButton, ib_nextButton;
        TextView tv_namaMusic;
        ImageButton ib_playButton, ib_backButton, ib_nextButton;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
    
            tv_namaMusic = findViewById(R.id.tv_namaMusic);//Menyambungkan view dengan java
            ib_playButton = findViewById(R.id.ib_playButton);//Menyambungkan view dengan java
            ib_backButton = findViewById(R.id.ib_backButton);//Menyambungkan view dengan java
            ib_nextButton = findViewById(R.id.ib_nextButton);//Menyambungkan view dengan java
            
        }
    }
    ```

7. Setelah Menyambungkan view yang ada ke file javanya, sekarang kita akan membuat code java untuk tombol dan logika dari Music Player sederhana. Untuk membuat codenya silahkan ikuti code dibawah.

    ```java
    package com.example.itc;
    
    import androidx.appcompat.app.AppCompatActivity;
    
    import android.annotation.SuppressLint;
    import android.media.Image;
    import android.os.Bundle;
    import android.view.View;
    import android.widget.ImageButton;
    import android.widget.TextView;
    
    public class MainActivity extends AppCompatActivity {
    
        private String[] songName = {"Lagu 1", "Lagu 2", "Lagu 3"};
        private int trackSong = 0;
        private boolean playing = true;
        TextView tv_namaMusic;
        ImageButton ib_playButton, ib_backButton, ib_nextButton;
    
        @SuppressLint("SetTextI18n")
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
    
            tv_namaMusic = findViewById(R.id.tv_namaMusic);//Menyambungkan view dengan java
            ib_playButton = findViewById(R.id.ib_playButton);//Menyambungkan view dengan java
            ib_backButton = findViewById(R.id.ib_backButton);//Menyambungkan view dengan java
            ib_nextButton = findViewById(R.id.ib_nextButton);//Menyambungkan view dengan java
    
            tv_namaMusic.setText("Playing " + songName[trackSong]);//Mengubah Nama Lagu
    
            ib_playButton.setOnClickListener(new View.OnClickListener() {//Membuat Fungsi Tombol
                @SuppressLint("UseCompatLoadingForDrawables")
                @Override
                public void onClick(View view) {//Pada saat klik
                    if (playing) {
                        ib_playButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_pause_24));
                        //Mengubah gambar menjadi pause
                        tv_namaMusic.setText("Stopped");//Mengubah Nama Lagu
                        playing = false;
                    } else {
                        ib_playButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_play_arrow_24));
                        //Mengubah gambar menjadi play
                        tv_namaMusic.setText("Playing " + songName[trackSong]);//Mengubah Nama Lagu
                        playing = true;
                    }
                }
            });
    
            ib_nextButton.setOnClickListener(new View.OnClickListener() {//Membuat Fungsi Tombol
                @Override
                public void onClick(View view) {//Pada saat klik
                    if (trackSong < 2)
                        trackSong = trackSong + 1;//Melanjutkan Lagu
                    else
                        trackSong = 0;//Kembali ke lagu awal
                    tv_namaMusic.setText("Playing " + songName[trackSong]);//Mengubah Nama Lagu
                }
            });
    
            ib_backButton.setOnClickListener(new View.OnClickListener() {//Membuat Fungsi Tombol
                @Override
                public void onClick(View view) {//Pada saat klik
                    if (trackSong > 0)
                        trackSong = trackSong - 1;//Membalikan Lagu
                    else
                        trackSong = 2;//Kembali ke lagu akhir
                    tv_namaMusic.setText("Playing " + songName[trackSong]);//Mengubah Nama Lagu
                }
            });
        }
    }
    ```
8. Jika semua berjalan dengan benar maka tampilan aplikasi saat dijalankan akan seperti dibawah.

    <img src="https://cdn.discordapp.com/attachments/785838698293493761/790169752496504872/unknown.png" alt="Contoh Hasil" width="250"/>
    
    > Tampilan akan menjadi seperti itu apabila proses dijalankan dengan benar.

 - Pada saat tombol play di klik maka aplikasi akan menampilkan tulisan stopped yang berarti berhenti dan pada saat ditekan kembali maka tulisan akan berubah kembali ke awal.
 - Jika user menekan tombol lanjut maka list lagu akan lanjut ke list berikutnya.
 - Jika user menekan tombol kembali maka list lagu akan kembali ke list berikutnya.

