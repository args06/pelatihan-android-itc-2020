# Proyek Akhir
Pada pelatihan sertifikasi ini akan ada proyek akhir agar kami dapat mengetahui tingkat kompetensi yang diperoleh setelah melakukan pembelajaran dari pertemuan pertama hingga akhir. Harapannya dengan proyek akhir ini kita dapat meningkatkan skill lebih dalam dan luas dari apa yang diberikan pada materi di setiap pertemuan. Tetap Semangaaatt~~

### Ketentuan Proyek
1. Tidak diperbolehkan melakukan plagiasi.
2. Proyek dilakukan secara individu.
3. Penilaian proyek dilakukan dengan cara presentasi secara terjadwal.
4. Presentasi dilakukan pada tanggal 19 Februari 2021.
5. Informasi lebih lanjut akan diumumkan melalui forum.

### Syarat Proyek
1. **Improvisasi sangat dianjurkan**, namun tetap pada minimum standar persyaratan.
2. Proyek Belajar dapat dijadikan contoh, namun tidak untuk melakukan plagiasi.
2. Materi pada proyek **minimal** seperti berikut :
    - View
    - Integrasi Java
    - Intent Explicit
    - Recycler View
    - API
2. Untuk **jumlah activity** yang ada di dalam project **minimal** menggunakan **3 halaman** yang terdiri dari **2 halaman wajib** yaitu Home sama Detail dan **1 halaman bebas** sesuai dengan kreativitas masing-masing.
3. Desain harus **menerapkan materi ui** yang dibuat menarik, layak, pemilihan warna yang sesuai dan bagus.
4. Untuk penggunaan API **harus memilih** dari 3 API yang telah disediakan, yaitu :
    - SportDB : https://www.thesportsdb.com/api.php
    - GithubAPI : https://api.github.com/
    - ThemealDB : https://www.themealdb.com/api.php
5. Untuk penggunaan GithubAPI, ada beberapa endpoint yang dapat digunakan untuk mengambil data seperti berikut ini,
    - Search : https://api.github.com/search/users?q={username}
    - Detail user : https://api.github.com/users/{username}
    - List Follower : https://api.github.com/users/{username}/followers
    - List Following : https://api.github.com/users/{username}/following

#### Penjelasan Activity

1. **Halaman HomeActivity**
    - Menampilkan Informasi dalam format List menggunakan Recycler View.
    - Memunculkan halaman detail ketika salah satu item ditekan.

2. **Halaman DetailActivity**
    - Menampilkan gambar dan informasi yang relevan pada halaman detail.
    - Menambahkan menu back pada action bar.
3. **Bebas**
    - Bebas